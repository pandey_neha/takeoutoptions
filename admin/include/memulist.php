<div class="manubox_padding">
<div class="urbangreymenu">

<h3 class="headerbar"><a href="">Home</a></h3>
<ul class="submenu">
<li><a href="home-addnew.php" >EDirectory Add New </a></li>
<li><a href="home.php" >EDirectory Show List</a></li>
<li><a href="takeoutstep-addnew.php" >TAKEOUT OPTION Add New </a></li>
<li><a href="takeoutstep.php" >TAKEOUT OPTION Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Email Content</a></h3>
<ul class="submenu">
<li><a href="email_accountactivation.php" >Email Activation For Customer</a></li>
<li><a href="email_accountactivationres.php" >Activation For Basic Listing</a></li>
<li><a href="email_appovebyadmin.php" >After Approve For Basic Listing</a></li>
<li><a href="email_activationprofessional.php" >Activation For Prof Listing</a></li>
<li><a href="email_sendpassword.php" >Email Send Password</a></li>
<li><a href="email_forgotpassword.php" >Email Forgot Password</a></li>
<li><a href="email_newsletteractive.php" >Email Newsletter Activation</a></li>
</ul>
<h3 class="headerbar"><a href="">Join Success Content</a></h3>
<ul class="submenu">
<li><a href="joinsucess-addnew.php" >Join Success User Content</a></li>
<li><a href="joinsucessowner-addnew.php" >Join Success Owner Content</a></li>
</ul>
<h3 class="headerbar"><a href="">Join Page Pop Up</a></h3>
<ul class="submenu">

<li><a href="join_popup-addnew.php" >Join Page Pop Up Add New</a></li>
<li><a href="compareplan-addnew.php" >Compare Plan Add New</a></li>
</ul>
<h3 class="headerbar"><a href="">User List</a></h3>
<ul class="submenu">
<li><a href="users.php?p=email" >Awaiting For Email Approval</a></li>
<li><a href="users.php?p=live" >All Active User</a></li>
<li><a href="users.php?p=block" >All Block User</a></li>
</ul>
<h3 class="headerbar"><a href="">Restaurant List</a></h3>
<ul class="submenu">
<li><a href="restaurant.php?p=email" >Awaiting For Email Approval</a></li>
<li><a href="restaurant.php?p=awaiting" >Awaiting For Admin Approval</a></li>
<li><a href="restaurant.php?p=live" >All Active Restaurant</a></li>
<li><a href="restaurant.php?p=block" >All Block Restaurant</a></li>
</ul>
<h3 class="headerbar"><a href="">Cuisine</a></h3>
<ul class="submenu">
<li><a href="cuisine-addnew.php" >Add New </a></li>
<li><a href="cuisine.php" >Show List</a></li>
</ul>

<h3 class="headerbar"><a href="">State List</a></h3>
<ul class="submenu">
<li><a href="state-addnew.php" >Add New </a></li>
<li><a href="state.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Payment Option List</a></h3>
<ul class="submenu">
<li><a href="paymentoption-addnew.php" >Add New </a></li>
<li><a href="paymentoption.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Ambiance List</a></h3>
<ul class="submenu">
<li><a href="ambiance-addnew.php" >Add New </a></li>
<li><a href="ambiance.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Price Range List</a></h3>
<ul class="submenu">
<li><a href="pricerange-addnew.php" >Add New </a></li>
<li><a href="pricerange.php" >Show List</a></li>
</ul>



<h3 class="headerbar"><a href="">Faq</a></h3>
<ul class="submenu">
<li><a href="faq-addnew.php" >Add New </a></li>
<li><a href="faq.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Terms</a></h3>
<ul class="submenu">
<li><a href="terms-addnew.php" >Add New </a></li>
<li><a href="terms.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Privacy</a></h3>
<ul class="submenu">
<li><a href="privacy-addnew.php" >Add New </a></li>
<li><a href="privacy.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Contact Us</a></h3>
<ul class="submenu">
<li><a href="contactus_bar-addnew.php" >Add New </a></li>
<li><a href="contactus_bar.php" >Show List</a></li>
</ul>
<h3 class="headerbar"><a href="about-us.php">News letter</a></h3>
<ul class="submenu">
<li><a href="newsletter-addnew.php" >News letter Add New</a></li>
<li><a href="newsletter.php">News letter Content Show List</a></li>
<li><a href="subscriberlist.php">Subscriber Showlist Show List</a></li>
</ul>
<h3 class="headerbar"><a href="">Banner</a></h3>
<ul class="submenu">
<li><a href="indexbanner-addnew.php" >Index banner Addnew</a></li>
<li><a href="indexbanner.php">Index banner Show</a></li>
</ul>

<h3 class="headerbar"><a href="">Adsence</a></h3>
<ul class="submenu">
<li><a href="adsence-addnew.php" >Add New</a></li>
<li><a href="adsence.php">Show List</a></li>
</ul>

<h3 class="headerbar"><a href="">CSV</a></h3>
<ul class="submenu">
<li><a href="upload_csv.php" >Upload CSV</a></li>
<li><a href="res_by_csv.php">Show List</a></li>
</ul>

</div>
</div>