<?php
include("conn.php");
include("access.php");
?>
<?php
$sql_generalSettings="select * from ".$prev."general_settings";
$re_generalSettings=mysql_query($sql_generalSettings);
$d_generalSettings=mysql_fetch_array($re_generalSettings);
$sql_adminuser="select * from ".$prev."adminuser";
$re_adminuser=mysql_query($sql_adminuser);
$d_adminuser=mysql_fetch_array($re_adminuser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To whyiteasy Admin Control Panel</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 6]>
    <link rel="stylesheet" type="text/css" href="css/style_IE6.css" />
    <![endif]-->
<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript" src="js/ddaccordion.js">

/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/

</script>
<script type="text/javascript">

ddaccordion.init({
	headerclass: "headerbar", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: true, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", "selected"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "normal", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})

</script>
<script language="javascript">
function opentexarea(divID,value)
{
if(value==1)
{
document.getElementById(divID).style.display='';
}
else
{
document.getElementById(divID).style.display='none';
}
}
function openclosepdf(divID,value)
{

if(value==0)
{
document.getElementById(divID).style.display='';
}
else
{
document.getElementById(divID).style.display='none';
}
}



</script>
</head>

<body onload="opentexarea('colseDivID','<?=$d_generalSettings['IsSite_Setting_Close']?>');openclosepdf('colseregistrationDivID','<?=$d_generalSettings['DefaultRegistrationStatus']?>')">
<div id="container">
<div style="width:195px; float:left">
<?php include("include/left_slogan.php")?>
<?php include("include/profile_pic.php")?>
<div>
<?php include("include/memulist.php")?>
</div>
</div>
<div style="width:801px; float:right">
<div style="width:801px; margin-top:10px;">
<div class="box_header"></div>
<div class="box_middle">
<div style="margin:20px 20px 10px 20px ; padding:10px 10px; border:#FFFFFF 1px solid; background:#e4e6e8;"><h1 style="text-align:left;">General Settings</h1></div>
<div style="margin:0px 20px;"><p></p></div>
<form name="generalSettings"  method="post" action="insert_general_settings.php" enctype="multipart/form-data">
<div style="margin:10px 25px 0px 25px; width:751px;">

 <div class="page_box_edit_header"><h2>Update Your Admin Settings</h2></div>
  <table width="751" border="0" cellpadding="0" cellspacing="0">
  <tr class="page_box_edit_middle">
    <td  colspan="2" align="center" class="" style="padding-right:10px; color:#FF0000; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px;">
	<?php
	if($_SESSION['join_UUerror']=='500')
	{
	unset($_SESSION['join_UUerror']);
	?>
	Please check password
	<?php
	}
	?>
	<?php
	if($_SESSION['join_EEerror']=='300')
	{
	unset($_SESSION['join_EEerror']);
	?>
	Please enter correct email address
	<?php
	}
	?>
	<?php
	if(isset($_SESSION['password_change']))
	{
	?>
	Your Password is successfully changed
	<?php
	}
	?>
	</td>
 
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Name:</td>
   <td width="531" align="left" valign="middle" class="page_row_link"><input type="text" name="adminName" value="<?=$d_generalSettings['adminName']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">User Name:</td>
   <td width="531" align="left" valign="middle" class="page_row_link"><input type="text" name="userName"
    value="<?=$d_adminuser['admin_userName']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Email:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="adminEmailAddress" value="<?=$d_generalSettings['adminEmailAddress']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;"><strong>New Password:</strong>
	</td>
    <td width="531" align="left" valign="middle" class="page_row_link"><input type="password" name="adminnewPassword"  class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="left" class="" style="padding-left:10px;" colspan="2">(Leave the password fields blank to retain old password)
	</td>
   
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;"><strong>New Password (again):</strong></td>
   <td width="531" align="left" valign="middle" class="page_row_link"><input type="password" name="ReadminnewPassword" value="" class="text_box_midium" /></td>
    
  </tr>
  
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;"><strong>Site Logo:</strong></td>
   <td width="531" align="left" valign="middle" class="page_row_link">
 
   <?php
   if($d_generalSettings['logo']!='')
   {
   ?>
   <img src="../<?=$d_generalSettings['logo']?>" height="70" width="150" /> 
   <br />

   <?php
   }
   ?>
   <input type="hidden" name="picturepath" value="<?=$d_generalSettings['logo']?>" />
   <input type="file" name="picture"  />
   </td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;"><strong>Default Restaurant Logo:</strong></td>
   <td width="531" align="left" valign="middle" class="page_row_link">
 
   <?php
   if($d_generalSettings['reslogopicture']!='')
   {
   ?>
   <img src="../<?=$d_generalSettings['reslogopicture']?>" height="150" width="150" /> 
   <br />

   <?php
   }
   ?>
   <input type="hidden" name="reslogopicturepath" value="<?=$d_generalSettings['reslogopicture']?>" />
   <input type="file" name="reslogopicture"  />
   </td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Professional Listing Fees:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   $<input type="text" name="professionalListingFees" value="<?=$d_generalSettings['professionalListingFees']?>" class="text_box_midium" /></td>
    
  </tr>
  
   <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Freature Listing Fees:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   $<input type="text" name="featuresFees" value="<?=$d_generalSettings['featuresFees']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Freature Listing Duration:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="featuresDays" value="<?=$d_generalSettings['featuresDays']?>" class="text_box_midium" style="width:100px;" /> Days</td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;"><strong>Google analytic:</strong></td>
   <td width="531" align="left" valign="middle" class="page_row_link">
      <textarea name="googleanalytic" cols="40" rows="8"><?=$d_generalSettings['googleanalytic']?></textarea>
    
   </td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Paypal Email:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="paypal_Email" value="<?=$d_generalSettings['paypal_Email']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Contact Email:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="Contact_Email" value="<?=$d_generalSettings['Contact_Email']?>" class="text_box_midium" /></td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Contact Email Name:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="contactEmailName" value="<?=$d_generalSettings['contactEmailName']?>" class="text_box_midium" /></td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Site Url:</td>
   <td width="531" align="left" valign="middle" class="page_row_link">
   <input type="text" name="siteurl" value="<?=$d_generalSettings['siteurl']?>" class="text_box_midium" /></td>
    
  </tr>
  
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Site Title :</td>
    <td width="531" align="left" valign="middle" class="page_row_link"><input type="text" name="SiteTitle" value="<?=$d_generalSettings['SiteTitle']?>"  class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Site Slogan:</td>
   <td width="531" align="left" valign="middle" class="page_row_link"><input type="text" name="SiteSlogan" value="<?=$d_generalSettings['SiteSlogan']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Site Meta Keys :</td>
    <td width="531" align="left" valign="middle" class="page_row_link"><input type="text" name="SiteMetaKeys" value="<?=$d_generalSettings['SiteMetaKeys']?>" class="text_box_midium" /></td>
    
  </tr>
  <tr class="page_box_edit_middle">
    <td width="220" align="right" class="title_text" style="padding-right:10px;">Site Meta Descriptions :</td>
    <td width="531" align="left" valign="middle" class="page_row_link">
	<textarea name="SiteMetaDescriptions" style="width:310px; height:75px;" ><?=$d_generalSettings['SiteMetaDescriptions']?></textarea></td>
    
  </tr>

  
  
  
  <tr class="page_box_edit_middle">
    <td width="220" align="center" class="title_text" style="padding-right:10px;" colspan="2">
	<img src="images/spacer.gif" height="10" />
	</td>
    </tr>
  <tr class="page_box_edit_middle">
    <td align="center" colspan="2">
	<input type="submit" name="submit" value="Save" class="save" />
	</td>
    
  </tr>
  
  <tr class="page_box_edit_bottom"><td colspan="2">&nbsp;</td></tr>
</table>





<div class="spacer"></div>
</div>
</form>
<div class="spacer"></div>
</div>
<div class="box_bottom"></div>
<div class="spacer"></div>

<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
</body>
</html>
