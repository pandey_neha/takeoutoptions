<?php
include("conn.php");
include("access_afterlogon.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome To Takeout Option</title>
<link href="css/takeout.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript">
function myFocus(element) {
    if (element.value == element.defaultValue) {
      element.value = '';
    }
  }
   function myBlur(element) {
     if (element.value == '') {
       element.value = element.defaultValue;
    }
}
</script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
function setzone()

{

var offset = new Date().getTimezoneOffset();
document.getElementById('zoneoffset').value=offset;
$(".submit_button").removeAttr("disabled");
}


</script>
</head>

<body  onload="setzone();">



<?php include("include/header.php")?>






<!--MAIN_BODY-->
<div id="main_body">
<div class="login_border_div">
<div class="returning_customer">
<div class="faqs_headline"><h3>Returning <font>Customer -</font></h3></div>
 <form name="login" method="post" action="<?=$basepath?>login_check.php">
<div class="login_form">
<div style="padding:4px 0px 0px 0px;" id="loginerror">
								<?php
								if(isset($_SESSION['login_error'])==100)
								{
								unset($_SESSION['login_error']);
								?>
								<p >Please Enter Your Email</p>
								<?php
								}
								elseif(isset($_SESSION['login_error'])==200)
								{
								unset($_SESSION['login_error']);
								?>
								<p >Please Enter Your Password</p>
								<?php
								}
								elseif(isset($_SESSION['loginErrordetection'])==2)
								{
								unset($_SESSION['loginErrordetection']);
								?>
								<p >Your account is not activated yet. Please check mail and activate your account</p>
								<?php
								}
								elseif(isset($_SESSION['loginErrordetection'])==3)
								{
								unset($_SESSION['loginErrordetection']);
								?>
								<p >Your account is blocked by admin. Please contact administrator.</p>
								<?php
								}
								elseif(isset($_SESSION['loginErrordetection'])==1)
								{
								unset($_SESSION['loginErrordetection']);
								?>
								<p >Wrong Email or Password.</p>
								<?php
								}

								?>
								</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30%" align="left" valign="top">Email: <span>*</span></td>
    <td width="70%"><input name="emailAddress" type="text" value="<?=$_SESSION['cusemailAddress']?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <td align="left" valign="top">Password: <span>*</span></td>
    <td><input name="userPassword" type="password" value="<?=isset($passsabya)?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <td valign="middle"></td>
    <td valign="middle"><a href="forgot-password.php" class="meroon_link">Forgot your Password?</a>
    <br /><input name="remember" type="checkbox" value="Y" checked id="receive" /><label for="receive">Remember me on this computer</label></td>
  </tr>
  <tr>
    <td></td>
    <td>
	<input type="hidden" name="zoneoffset" id="zoneoffset"  value="0" />
	<input type="submit" name="search" value="LOGIN" class="submit_button" disabled="disabled" /></td>
  </tr>
</table>


<div class="spacer"></div>
</div>
</form>
<div class="spacer"></div>
</div>

<div class="first_user_div">
<div class="first_user_headline"><h3>First time <font>user? -</font></h3></div>
<div class="create_btn_div"><a href="register.php" class="create_account_div">Create Account</a></div>
<div class="create_account_details_text">View past orders, easy reordering, rate orders, write reviews, get CashCoupon and more...</div>

<div class="spacer"></div>
</div>



<div class="spacer"></div>
</div>

<?php
include("include/cuisine_panel.php");
?>


<div class="spacer"></div>
</div>
<!--END-MAIN_BODY-->



<!--FOOTER-->
<?php include("include/footer.php")?>
<!--END-FOOTER-->






</body>
</html>
