<?php
function delascii($title)
{
$title=trim($title);
$title=str_replace(htmlentities("'",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("\"",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("\"",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("/",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities(">",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("<",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("]",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("[",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("}",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("{",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities(":",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("?",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("\\",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("+",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("=",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("*",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("&",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("^",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("%",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("$",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("#",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("@",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("!",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("`",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("~",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities("|",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities(";",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities(",",ENT_QUOTES),'',$title);
$title=str_replace(htmlentities(".",ENT_QUOTES),'',$title);
$var="-";
$var1="--";
for($i=0;$i<=100;$i++)
{
if(strpos($title,htmlentities($var1,ENT_QUOTES)))
{
$title=str_replace(htmlentities($var1,ENT_QUOTES),'-',$title);
}

}

return $title;

}
function delLongword($title)
{
$arr=array();
$arr=explode(" ",$title);
$avoy="";
foreach($arr as $val => $value)
{
	if(strlen($value)<15)
	{
		$avoy.=$value." ";
	}
}

  return $avoy;
}

function islongword($title)
{
$arr=array();
$arr=explode(" ",$title);
$avoy="";
foreach($arr as $val => $value)
{
	if(strlen($value)>15)
	{
		$avoy="1";
		
		break;
	}
}

  return $avoy;
}


function substr_words ($paragraph, $num_words) {
  $paragraph = explode (' ', $paragraph);
  $paragraph = array_slice ($paragraph, 0, $num_words);
  return implode (' ', $paragraph);
}
function fetwordfromurl($title)
{
$arr=array();
$title=trim($title);
$title=str_replace('/',' ',$title);
$title=str_replace('-',' ',$title);
$arr=explode(' ',$title);
foreach($arr as $val => $value)
{
if(ctype_alpha($value))
{
	$avoy.=ucwords($value)." ";
}
}
return $avoy;
}

function makeurl($title)
{
$arr=array();
$title=trim($title);
$title=str_replace(' ','-',$title);
$title=strtolower($title);

return $title;
}

function count_words($str)
{
$no = count(explode(" ",$str));
return $no;
}
function makeRandomString($max=6) {
    $i = 0; //Reset the counter.
    $possible_keys = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $keys_length = strlen($possible_keys);
    $str = ""; //Let's declare the string, to add later.
    while($i<$max) {
        $rand = mt_rand(1,$keys_length-1);
        $str.= $possible_keys[$rand];
        $i++;
    }
    return $str;
}
function get_timespan_string($date1,$date2) {


$diff = abs(strtotime($date2) - strtotime($date1));

$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

printf("%d years, %d months ", $years, $months, $days);
} 
function resizeMarkup($markup)
{
$w = 250;
$h = 250;

$patterns = array();
$replacements = array();
if( !empty($w) )
{
$patterns[] = '/width="([0-9]+)"/';
$patterns[] = '/width:([0-9]+)/';

$replacements[] = 'width="'.$w.'"';
$replacements[] = 'width:'.$w;
}

if( !empty($h) )
{
$patterns[] = '/height="([0-9]+)"/';
$patterns[] = '/height:([0-9]+)/';

$replacements[] = 'height="'.$h.'"';
$replacements[] = 'height:'.$h;
}

return preg_replace($patterns, $replacements, $markup);
}


function resizeMarkup1($markup)
{

$w = 470;
$h = 300;

$patterns = array();
$replacements = array();
if( !empty($w) )
{
$patterns[] = '/width="([0-9]+)"/';
$patterns[] = '/width:([0-9]+)px"/';


$replacements[] = 'width="'.$w.'"';
$replacements[] = 'width:'.$w;

}

if( !empty($h) )
{
$patterns[] = '/height="([0-9]+)"/';
$patterns[] = '/height:([0-9]+)px"/';


$replacements[] = 'height="'.$h.'"';
$replacements[] = 'height:'.$h;
}

return preg_replace($patterns, $replacements, $markup);
}
function selfURL() { $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; }
function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }


function timeCheck($to=0,$tom='AM',$fr=0,$frm='AM')
{

$current_hour=date("g");
$current_meri=date("A");
if($to=='' ||  $tom==''  ||  $fr==''  ||  $frm=='')
{
$val=0;
}
elseif($current_meri=='AM' && $frm=='AM' && $fr<=$current_hour )
{
$val=0;
}
elseif($current_meri=='PM' && $frm=='PM' && $fr<=$current_hour )
{
$val=0;
}
elseif($current_meri=='AM' && $tom=='AM' && $to>$current_hour )
{
$val=0;
}
elseif($current_meri=='PM' && $tom=='PM' && $to>$current_hour )
{
$val=0;
}
elseif($current_meri=='PM' && $tom=='AM' && $frm=='AM')
{
$val=0;
}
elseif($current_meri=='AM' && $tom=='PM' && $frm=='PM')
{
$val=0;
}
else
{
$val=1;
}


return $val;
}

function getLocalTimezone($offset=0)
{
    
    $zonelist = 
    array
    (
        'Kwajalein' => -12,
        'Pacific/Midway' => -11,
        'Pacific/Honolulu' => -10,
        'America/Anchorage' => -9,
        'America/Los_Angeles' => -8,
        'America/Denver' => -7,
        'America/Tegucigalpa' => -6,
        'America/New_York' => -5,
        'America/Caracas' => -4.5,
        'America/Halifax' => -4,
        'America/St_Johns' => -3.5,
        'America/Argentina/Buenos_Aires' => -3,
        'America/Sao_Paulo' => -3,
        'Atlantic/South_Georgia' => -2,
        'Atlantic/Azores' => -1,
        'Europe/Dublin' => 0,
        'Europe/Belgrade' => 1,
        'Europe/Minsk' => 2,
        'Asia/Kuwait' => 3,
        'Asia/Tehran' => 3.3,
        'Asia/Muscat' => 4,
        'Asia/Yekaterinburg' => 5,
        'Asia/Kolkata' => 5.5,
        'Asia/Katmandu' => 5.75,
        'Asia/Dhaka' => 6,
        'Asia/Rangoon' => 6.5,
        'Asia/Krasnoyarsk' => 7,
        'Asia/Brunei' => 8,
        'Asia/Seoul' => 9,
        'Australia/Darwin' => 9.5,
        'Australia/Canberra' => 10,
        'Asia/Magadan' => 11,
        'Pacific/Fiji' => 12,
        'Pacific/Tongatapu' => 13
    );
	
    $index = array_keys($zonelist,$offset);
    if(sizeof($index)!=1)
        return false;
    return $index[0];
}

function phone_number($value=0)
{
$value=str_replace(')','',$value);
$value=str_replace('(','',$value);
$value=str_replace('[','',$value);
$value=str_replace(']','',$value);
$value=str_replace('{','',$value);
$value=str_replace('}','',$value);
$fisrt=substr($value,0,3);
$second=substr($value,3,3);
$third=substr($value,6,4);

$total=$fisrt."-".$second."-".$third;
return $total;
}

function make_safe_url($string)
{
if(substr($string,0,4)=='http')
{
$newtext = explode(":", $string);
$string=$newtext[1];
}
return $string;
}

?>