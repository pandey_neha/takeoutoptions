<?php
include("conn.php");
include("accessowner_afterlogon.php");
if(!isset($_SESSION['ambiance']))
{
$_SESSION['ambiance']=array();
}
if(!isset($_SESSION['paymentoption']))
{
$_SESSION['paymentoption']=array();
}
if(!isset($_SESSION['cuisine']))
{
$_SESSION['cuisine']=array();
}
if(!isset($_SESSION['type']))
{
$proval=1;
}
else
{
$proval=$_SESSION['type'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome To Takeout Option</title>
<link href="css/takeout.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript">
function myFocus(element) {
    if (element.value == element.defaultValue) {
      element.value = '';
    }
  }
   function myBlur(element) {
     if (element.value == '') {
       element.value = element.defaultValue;
    }
}
</script>
<script type="text/javascript">

</script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
var geocoder = new google.maps.Geocoder();

function getState(zipcode,city,cityShort) {
    geocoder.geocode( { 'address': zipcode}, function (result, status) {
        var state = "N/A";
		var shortstate = "N/A";
        for (var component in result[0]['address_components']) {
            for (var i in result[0]['address_components'][component]['types']) {
                if (result[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
                  
					  shortstate = result[0]['address_components'][component]['short_name'];
					   state = result[0]['address_components'][component]['long_name'];
                    // do stuff with the state here!
				
                    document.getElementById(city).value = state;
					 document.getElementById(cityShort).value = shortstate;
                    return;
                }
            }
        }
    });
}
</script>

<script type="text/javascript" src="js/jquery.min.js"></script> 
<script src="js/join.js" type="text/javascript"></script>

<script type="text/javascript">

function checkNoOfCuisine(noofCuisine,cuisineCheck,type)
{
var count=0;
var val=parseInt(document.getElementById(noofCuisine).value);
for(var i=1;i<=val;i++)
{

if(document.getElementById(cuisineCheck+i).checked ==true)
{
if(count==2 && type==1)
{
document.getElementById(cuisineCheck+i).checked =false
alert("You can only choose up to 2 different cuisine types with Basic Listing");
return false;
}
else if(count==5 && type==2)
{
document.getElementById(cuisineCheck+i).checked =false
alert("You can only choose up to 5 different cuisine types with Professional Listing");
return false;
}
else
{
count++;
}
}
}


}

</script>
<script type="text/javascript">
function setzone()
{
var offset = new Date().getTimezoneOffset();
document.getElementById('zoneoffset').value=offset; 
}


</script>
</head>

<body onload="setzone();">



<?php include("include/header.php")?>




	


<!--MAIN_BODY-->
<div id="main_body">

<div class="main_body_left">
	<div class="compare_plan_top_area_div" style="padding-bottom:5px">

<div class="padding_sml">
<h1>Attention Restaurant Owners!</h1>
<h2>Do you want more exposure and more business?</h2>
</div>

<div class="padding_sml">
<h3>GET LISTED ON TAKEOUT OPTIONS TODAY!</h3>
<h2>Join now. It's <font>free</font>, and there are <font>no contracts</font> to sign.</h2>
</div>

<div class="padding_sml" style="padding:0;">
<h4>Attract new customers, Unlimited free exposure, Offer online ordering made easy!</h4>
<h5>We have two (2) options available to get your restaurant listed on our directory.</h5>
<span style="padding-top:40px; padding-left:10px">Choose one:</span>
</div>

       <?php
					
					$sql_profe_info="select * from ".$prev."join_popup ";
					$data_sql_profe_info=mysql_query($sql_profe_info);
					$data_value_profe_info=mysql_fetch_array($data_sql_profe_info);
					?>

<div class="spacer"></div>
</div>
	<form name="join" method="post" action="insert_join.php" enctype="multipart/form-data">
	
	<div>
        
      <?php
					if(!empty($_SESSION['join_failed']))
					{
						
						?>
						<div class="warnings">
						<h1>Registration Errors</h1>
						<ol>
						<?php
						if($_SESSION['restaurantName_error']=='100')
						{
						unset($_SESSION['restaurantName_error']);
						?>
						<li>Restaurant Name is required.</li>
			
						<?php
						}
						if($_SESSION['phoneNumber_error']=='100')
						{
						unset($_SESSION['phoneNumber_error']);
						?>
						<li>Phone No field is required.</li>
			
						<?php
						}
						elseif($_SESSION['phoneNumber_error']=='200')
						{
						unset($_SESSION['phoneNumber_error']);
						?>
						<li>Please use correct format for Phone No. e.g: 415-444-4444.</li>
			
						<?php
						}
						if($_SESSION['address_error']=='100')
						{
						unset($_SESSION['address_error']);
						?>
						<li>Address  field is required.</li>
			
						<?php
						}
						if($_SESSION['city_error']=='100')
						{
						unset($_SESSION['city_error']);
						?>
						<li>City   field is required.</li>
			
						<?php
						}
						if($_SESSION['country_error']=='100')
						{
						unset($_SESSION['country_error']);
						?>
						<li>Please Select Country.</li>
			
						<?php
						}
						if($_SESSION['state_error']=='100')
						{
						unset($_SESSION['state_error']);
						?>
						<li>Please Select a State.</li>
			
						<?php
						}
						if($_SESSION['postalCode_error']=='100')
						{
						unset($_SESSION['postalCode_error']);
						?>
						<li>Zip Code is Required.</li>
			
						<?php
						}
						
						if($_SESSION['cuisine_error']=='100')
						{
						unset($_SESSION['cuisine_error']);
						?>
						<li>Please Select Atleast One Cuisine .</li>
			
						<?php
						}
						if($_SESSION['onlineOrderUrl_error']=='100')
						{
						unset($_SESSION['onlineOrderUrl_error']);
						?>
						<li>Online menu url  is required .</li>
			
						<?php
						}
						if($_SESSION['ambiance_error']=='100')
						{
						unset($_SESSION['ambiance_error']);
						?>
						<li>Please Select Atleast One Ambiance .</li>
			
						<?php
						}
						
						if($_SESSION['pdf_error']=='100')
						{
						unset($_SESSION['pdf_error']);
						?>
						<li>Please Upload PDF in pdf format.</li>
			
						<?php
						}
						if($_SESSION['logo_error']=='100')
						{
						unset($_SESSION['logo_error']);
						?>
						<li>Please Upload logo in jpg,png,gif format.</li>
			
						<?php
						}
						if($_SESSION['image_error']=='100')
						{
						unset($_SESSION['image_error']);
						?>
						<li>Please Upload images in jpg,png,gif format.</li>
			
						<?php
						}
						if($_SESSION['userName_error']=='100')
						{
						unset($_SESSION['userName_error']);
						?>
						<li>The Username   field is required.</li>
			
						<?php
						}
						elseif($_SESSION['userName_error']=='200')
						{
						unset($_SESSION['userName_error']);
						?>
						<li>The Username  "<?=$_SESSION['userName']?>" is already registered with us. Please try with another one. </li>
			
						<?php
						}
						if($_SESSION['join_EEerror']=='100')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>The Account manager email ID  field is required.</li>
			
						<?php
						}
						elseif($_SESSION['join_EEerror']=='200')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>Please Check  Account manager email ID .</li>
			
						<?php
						}
						elseif($_SESSION['join_EEerror']=='300')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>The Account manager email ID  "<?=$_SESSION['accountEmailID']?>" is already registered with us. Please try with another one. </li>
			
						<?php
						}
						if($_SESSION['emailID_error']=='100')
						{
						unset($_SESSION['emailID_error']);
						?>
						<li>Restaurant email ID  field is required.</li>
			
						<?php
						}
						elseif($_SESSION['emailID_error']=='200')
						{
						unset($_SESSION['emailID_error']);
						?>
						<li>Please Check  Restaurant email ID .</li>
			
						<?php
						}
						
						if($_SESSION['userpassword_error']=='100')
						{
						unset($_SESSION['userpassword_error']);
						?>
						<li>Password field is required. </li>
						<?php
						}
						if($_SESSION['conuserpassword_error']=='100')
						{
						unset($_SESSION['conuserpassword_error']);
						?>
						<li>Please Re enter Password </li>
						<?php
						}
						else if($_SESSION['conuserpassword_error']=='200')
						{
						unset($_SESSION['conuserpassword_error']);
						?>
						<li>Password and Re-enter Password Mismatch!!</li>
						<?php
						}
						
						?>
						</ol>
						</div>
						<?php
						unset($_SESSION['join_failed']);
					}
					
					?>
                    
               
					
        <div class="form_main_div basic_listing">
		<input type="hidden" name="" id="freelistCountID" value="<?=($_SESSION['type']+1)?>" />
       	  <div class="borderless_headline" style="cursor:pointer; " id="freelistclick" ><h1><span onclick="javascript:change_joinmethod('freebasiclisting','professionalListing','1','freelistCountID','professionalCountID'); ">Basic Listing <font>- Free</font></span> <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_0','0')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_0" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_0','0');" class="delete"></a></div><p><?=$data_value_profe_info['basic_listing']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></h1>
		  
		  
		  </div>
        	<div class="inner_form_main" id="freebasiclisting">
            <input type="hidden"  id="type" value="0" />
            	<?php
			if(isset($_SESSION['type'])==1)
			{
			?>
			<div style="margin:20px 0px 0px 20px; color:#FF0000; font-family:Verdana, Arial, Helvetica, sans-serif; text-align:
			c">Please Fill Out the form below to list your restaurant in our directory for <strong>Free</strong>!</div>
			<div style="margin:20px 0px;"><h5 style="text-align:left; margin-left:200px;">Restaurant Contact Information </h5></div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_avoy">
                  <tr>
                    <th width="30%" align="left" valign="middle">Restaurant Name : * <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_1','1')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_1" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_1','1');" class="delete"></a></div><p><?=$data_value_profe_info['restaurentName']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="restaurantName" type="text" value="<?=$_SESSION['restaurantName']?>" class="<?php if($_SESSION['restaurantName_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Phone number : * <span style="position:relative"><a href="javascript:void(0);" tabindex="100"   onclick="open_popup('popupID_2','2')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"      /></a>
		  <div class="popup_div" id="popupID_2" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_2','2');" class="delete"></a></div><p><?=$data_value_profe_info['phoneNumber']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span>
		   <h6>example 555-555-5555</h6>
		  </th>
                    <td width="70%" align="left" valign="middle"><input name="phoneNumber" type="text" value="<?=$_SESSION['phoneNumber']?>" class="<?php if($_SESSION['phoneNumber_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Address : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_3','3')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_3" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_3','3');" class="delete"></a></div><p><?=$data_value_profe_info['address']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="address" type="text" value="<?=$_SESSION['address']?>" class="<?php if($_SESSION['address_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
				  <tr>
                    <td width="30%" align="left" valign="middle">Address :</td>
                    <td width="70%" align="left" valign="middle"><input name="addressline" type="text" value="<?=$_SESSION['addressline']?>" class="form_searchfield" /></td>
                  </tr>
                  <tr>
				  
                    <th width="30%" align="left" valign="middle">City : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_4','4')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_4" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_4','4');" class="delete"></a></div><p><?=$data_value_profe_info['city']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="city" type="text" value="<?=$_SESSION['city']?>" class="<?php if($_SESSION['city_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
			<tr>
                    <th align="left" valign="middle">Country : *</th>
                    <td align="left" valign="middle">
					<select name="country" class="<?php if($_SESSION['country_error']){?>form_listmenu_error<?php } else{?>form_listmenu<?php }?>" style="width:250px;" >
					
					<?php
					$sql_select="select * from ".$prev."country order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					?>
					<option value="<?=$d_select['id']?>" <?php if($d_select['id']==$_SESSION['country']){?> selected="selected" <?php }?>><?=$d_select['title']?></option>
					<?php
					}
					?>
					</select></td>
                  </tr>	  
                  <tr>
                    <th align="left" valign="middle">State : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_5','5')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_5" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_5','5');" class="delete"></a></div><p><?=$data_value_profe_info['state']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td align="left" valign="middle">
					
						
				
				
					<select name="state" id="state" class="<?php if($_SESSION['state_error']){?>form_listmenu_error<?php } else{?>form_listmenu<?php }?>" >
				<option value="0"></option>	
				<?php	
				 $sql_select="select * from ".$prev."state where country='1' order by weight";
				 $re_select=mysql_query($sql_select);
				 while($d_select=@mysql_fetch_array($re_select))
				 {
				  ?>
				  <option  value="<?=$d_select['id']?>" <?php if($_SESSION['state']==$d_select['id']){?> selected="selected"<?php }?>><?=$d_select['title']?></option>
				<?php
				}
				?>	
				</select>
				
					</td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Zip Code : * <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_6','6')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_6" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_6','6');" class="delete"></a></div><p><?=$data_value_profe_info['zipcode']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="postalCode" type="text"  value="<?=$_SESSION['postalCode']?>"  class="<?php if($_SESSION['postalCode_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
<tr><td colspan="2"	><div style="margin:20px 0px;"><h5 style="text-align:left; margin-left:200px">Restaurant Details</h5></div></td></tr>
				  
                  <tr>
                    <th width="30%" align="left" valign="top">Cuisine : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_7','7')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_7" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_7','7');" class="delete"></a></div><p><?=$data_value_profe_info['cuisine']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span>
<h6>  Choose up to 2</h6>
        
          </th>
                    <td width="70%" align="left" valign="middle">
					<div style="width:460px">
					<?php
					$n=0;
					$sql_select="select * from ".$prev."cuisine order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					$n++;
					?>
             
					<div style="width:115px; float:left">
					<table cellpadding="0" cellspacing="0"  border="0" width="100%">
                          <tr>
                            <td width="10%" align="center" valign="middle"><input type="checkbox" name="cuisineCheck<?=$d_select['id']?>" id="cuisineCheck<?=$n?>" <?php if(in_array($d_select['id'],$_SESSION['cuisine'])){?> checked="checked" <?php }?> value="Y" onclick="checkNoOfCuisine('noofCuisine','cuisineCheck','1');" /></td>
                            <td width="90%" align="left" valign="middle"><label for="cuisineCheck<?=$n?>" style="cursor:pointer"><?=$d_select['title']?></label></td>
                            
                          </tr>
					</table>
					</div>	  
					<?php
					}
					?>
					<input type="hidden" id="noofCuisine" value="<?=$n?>" />
					<div class="spacer"></div>	
					</div>
					</td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="top">Include a  logo 
for your listing: <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_8','8')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_8" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_8','8');" class="delete"></a></div><p><?=$data_value_profe_info['logoyourlist']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="top">
					<?php
				   if($_SESSION['logo_avoy']!='')
				   {
				   ?>
				   <img src="<?=$_SESSION['logo_avoy']?>" height="100" width="100" /> 
				  
				   <?php
				   }
				   ?>
  
   
   <input type="hidden" name="logopath" value="<?=$_SESSION['logo_avoy']?>" />
  
					<input name="logo" type="file" /></td>
                  </tr>
                </table>
			<div style="margin:20px 0px;"><h5>Restaurant Owner Details </h5></div>	
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_avoy">    
                  <tr>
                    <th width="30%" align="left" valign="middle">Choose a username : *</th>
                    <td width="70%" align="left" valign="middle"><input name="userName" type="text" value="<?=$_SESSION['userName']?>"  class="<?php if($_SESSION['userName_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Choose password : *</th>
                    <td width="70%" align="left" valign="middle"><input name="userpassword" value="<?=$_SESSION['userpassword']?>" type="password" class="<?php if($_SESSION['userpassword_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
				  <tr>
                    <th width="30%" align="left" valign="middle">Re-enter Password : * </th>
                    <td width="70%" align="left" valign="middle"><input name="conuserpassword" value="<?=$_SESSION['conuserpassword']?>" type="password" class="<?php if($_SESSION['conuserpassword_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Account manager email ID : *</th>
                    <td width="70%" align="left" valign="middle"><input name="accountEmailID" value="<?=$_SESSION['accountEmailID']?>"  type="text" class="<?php if($_SESSION['join_EEerror']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                    <td width="70%" align="left" valign="middle">
					<input type="hidden" name="type" value="1"  id="type"/>
					<input name="" type="submit" value="submit" class="form_submit" /></td>
                  </tr>
                  
                </table>
			<div  class="help_avoy">Need More Information to display in our directory? Choose our Professional Listing bellow :</div>		
			<?php
			}
			?>
            </div>
        </div>
        
        
             
        
        <div class="form_main_div basic_listing">
		<input type="hidden" name="" id="professionalCountID" value="<?=$proval?>" />
       	    <div class="form_heading_text" id="professionalonclickID" style="cursor:pointer"><h2><span  onclick="javascript:change_joinmethod('professionalListing','freebasiclisting','2','freelistCountID','professionalCountID');">Professional Listing <font>- One time payment of $<?=$d_site_title['professionalListingFees']?></font></span> <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_100','100')"  ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_100" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_100',100);" class="delete"></a></div><p><?=$data_value_profe_info['profes_listing']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></h2></div>
        	<div class="inner_form_main" id="professionalListing">
			<?php
			if(isset($_SESSION['type'])==2)
			{
			?>
			<div style="margin:20px 0px 0px 20px; color:#FF0000; font-family:Verdana, Arial, Helvetica, sans-serif; text-align:
			c">Please Fill Out the form below to list your restaurant in our directory for $<?=$d_site_title['professionalListingFees']?>!</div>
			<div style="margin:20px 0px;"><h5 style="text-align:left; margin-left:200px">Restaurant Contact Information </h5></div>
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_avoy" >
                <tr>
                    <th width="30%" align="left" valign="middle">Restaurant Name : * <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_1','1')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_1" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_1','1');" class="delete"></a></div><p><?=$data_value_profe_info['restaurentName']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="restaurantName" type="text" value="<?=$_SESSION['restaurantName']?>" class="<?php if($_SESSION['restaurantName_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Phone number : * <span style="position:relative"><a href="javascript:void(0);" tabindex="100"   onclick="open_popup('popupID_2','2')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"      /></a>
		  <div class="popup_div" id="popupID_2" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_2','2');" class="delete"></a></div><p><?=$data_value_profe_info['phoneNumber']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span>
		   <h6>example 555-555-5555</h6>
		  </th>
                    <td width="70%" align="left" valign="middle"><input name="phoneNumber" type="text" value="<?=$_SESSION['phoneNumber']?>" class="<?php if($_SESSION['phoneNumber_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Address : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_3','3')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_3" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_3','3');" class="delete"></a></div><p><?=$data_value_profe_info['address']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="address" type="text" value="<?=$_SESSION['address']?>" class="<?php if($_SESSION['address_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
<tr>
                    <th width="30%" align="left" valign="middle">Address :</th>
                    <td width="70%" align="left" valign="middle"><input name="addressline" type="text" value="<?=$_SESSION['addressline']?>" class="form_searchfield" /></td>
                  </tr>				  
                  <tr>
                    <th width="30%" align="left" valign="middle">City : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_4','4')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_4" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_4','4');" class="delete"></a></div><p><?=$data_value_profe_info['city']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="city" type="text" value="<?=$_SESSION['city']?>" class="<?php if($_SESSION['city_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
				 <tr>
                    <th align="left" valign="middle">Country : *</th>
                    <td align="left" valign="middle">
					<select name="country" class="<?php if($_SESSION['country_error']){?>form_listmenu_error<?php } else{?>form_listmenu<?php }?>" style="width:250px;" >
					
					<?php
					$sql_select="select * from ".$prev."country order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					?>
					<option value="<?=$d_select['id']?>" <?php if($d_select['id']==$_SESSION['country']){?> selected="selected" <?php }?>><?=$d_select['title']?></option>
					<?php
					}
					?>
					</select></td>
                  </tr> 
				 <tr>
                    <th align="left" valign="middle">State : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_5','5')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_5" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_5','5');" class="delete"></a></div><p><?=$data_value_profe_info['state']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td align="left" valign="middle">
					
						
				
				
					<select name="state" id="state" class="<?php if($_SESSION['state_error']){?>form_listmenu_error<?php } else{?>form_listmenu<?php }?>" >
				<option value="0"></option>		
				<?php	
				 $sql_select="select * from ".$prev."state where country='1' order by weight";
				 $re_select=mysql_query($sql_select);
				 while($d_select=@mysql_fetch_array($re_select))
				 {
				  ?>
				  <option  value="<?=$d_select['id']?>" <?php if($_SESSION['state']==$d_select['id']){?> selected="selected"<?php }?>><?=$d_select['title']?></option>
				<?php
				}
				?>	
				</select>
				
					</td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Zip Code : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_6','6')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_6" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_6','6');" class="delete"></a></div><p><?=$data_value_profe_info['zipcode']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="postalCode" type="text"  value="<?=$_SESSION['postalCode']?>"  class="<?php if($_SESSION['postalCode_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
				  
<tr>

<td  colspan="2"><div style="margin:20px 0px;"><h5 style="text-align:left; margin-left:200px">Restaurant Details </h5></div></td>
</tr>				  
                  <tr>
                    <th width="30%" align="left" valign="top">Cuisine : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_7','7')" ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_7" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_7','7');" class="delete"></a></div><p><?=$data_value_profe_info['cuisine']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span>
		  <h6>Choose Upto 5</h6>
		  </th>
                    <td width="70%" align="left" valign="middle">
					<div style="width:460px">
					<?php
					$n=0;
					$sql_select="select * from ".$prev."cuisine order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					$n++;
					?>
					<div style="width:115px; float:left">
					<table cellpadding="0" cellspacing="0"  border="0" width="100%">
                          <tr>
                            <td width="10%" align="center" valign="middle"><input type="checkbox" name="cuisineCheck<?=$d_select['id']?>" id="cuisineCheck<?=$n?>" <?php if(in_array($d_select['id'],$_SESSION['cuisine'])){?> checked="checked" <?php }?> value="Y" onclick="checkNoOfCuisine('noofCuisine','cuisineCheck','2');" /></td>
                            <td width="90%" align="left" valign="middle"><label for="cuisineCheck<?=$n?>" style="cursor:pointer"><?=$d_select['title']?></label></td>
                            
                          </tr>
					</table>
					</div>	  
					<?php
					}
					?>
					<input type="hidden" id="noofCuisine" value="<?=$n?>" />
					<div class="spacer"></div>	
					</div>
					</td>
                  </tr>
                  
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">Dinning Room Hours : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_9','9')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_9" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_9','9');" class="delete"></a></div><p><?=$data_value_profe_info['diningroomHour']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">&nbsp;</td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                  <td width="70%" align="left" valign="middle">
       	        <table width="297" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="79" align="left" valign="middle">Monday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="dinningMondayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningMondayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							 <td width="39" align="center" valign="middle">
							 <select name="dinningMondayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningMondayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningMondayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
							 </td>
                          
                            <td width="61" align="center" valign="middle"><select name="dinningMondayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningMondayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningMondayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningMondayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningMondayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Tuesday</td>
                        
                            <td width="59" align="center" valign="middle">
							<select name="dinningTuesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningTuesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningTuesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningTuesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningTuesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                           
                            <td width="61" align="center" valign="middle"><select name="dinningTuesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningTuesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningTuesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningTuesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningTuesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Wednesday</td>
                          
                            <td width="59" align="center" valign="middle">
							<select name="dinningWednesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningWednesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningWednesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningWednesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningWednesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            
                            <td width="61" align="center" valign="middle"><select name="dinningWednesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningWednesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningWednesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningWednesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningWednesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Thursday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="dinningThursdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningThursdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningThursdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningThursdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningThursdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          
                            <td width="61" align="center" valign="middle"><select name="dinningThursdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningThursdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningThursdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningThursdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningThursdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Friday</td>
                          
                            <td width="59" align="center" valign="middle">
							<select name="dinningFridayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningFridayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningFridayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningFridayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningFridayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                           
                            <td width="61" align="center" valign="middle"><select name="dinningFridayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningFridayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningFridayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningFridayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningFridayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Saturday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="dinningSaturdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningSaturdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningSaturdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningSaturdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningSaturdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                           
                            <td width="61" align="center" valign="middle"><select name="dinningSaturdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningSaturdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningSaturdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningSaturdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningSaturdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Sunday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="dinningSundayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningSundayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningSaturdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningSaturdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningSaturdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          
                            <td width="61" align="center" valign="middle"><select name="dinningSundayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['dinningSundayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="dinningSundayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['dinningSundayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['dinningSundayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                    </table>

                    </td>
                  </tr>
                  
               	  <tr>
                    <th width="30%" align="left" valign="middle">Bar Available : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_10','10')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_10" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_10','10');" class="delete"></a></div><p><?=$data_value_profe_info['barAvailable']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">
                    <input name="barAvailabiliyCheck" id="barAvailabiliyCheckYes" type="radio" value="Y" <?php if($_SESSION['barAvailabiliyCheck']=='Y'){?> checked="checked" <?php }?> onclick="checkOpenClose('barAvailabiliyCheckYes','barAvailabiliyDivID');"  /> Yes 
                    <input name="barAvailabiliyCheck" id="barAvailabiliyCheckNo" type="radio" value="N"  <?php if($_SESSION['barAvailabiliyCheck']=='N'  || !$_SESSION['barAvailabiliyCheck'] ){?> checked="checked" <?php }?> onclick="checkOpenClose('barAvailabiliyCheckNo','barAvailabiliyDivID');" /> No
                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                  <td width="70%" align="left" valign="middle">
       	        <div id="barAvailabiliyDivID" <?php if($_SESSION['barAvailabiliyCheck']=='N'  || !$_SESSION['barAvailabiliyCheck']){?> style="display:none" <?php }?>>
				<table width="297" border="0" cellspacing="0" cellpadding="0">
				<tr>
                            <td colspan="5" align="left" valign="middle"><span>If Yes, Bar Hours -</span></td>
                    </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Monday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="barMondayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barMondayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barMondayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barMondayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barMondayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            
                            <td width="61" align="center" valign="middle"><select name="barMondayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barMondayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barMondayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barMondayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barMondayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Tuesday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="barTuesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barTuesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barTuesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barTuesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barTuesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
							 
                            
                            <td width="61" align="center" valign="middle"><select name="barTuesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barTuesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barTuesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barTuesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barTuesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Wednesday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="barWednesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barWednesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barWednesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barWednesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barWednesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            
                            <td width="61" align="center" valign="middle"><select name="barWednesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barWednesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barWednesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barWednesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barWednesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Thursday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="barThursdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barThursdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barThursdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barThursdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barThursdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          
                            <td width="61" align="center" valign="middle"><select name="barThursdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barThursdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barThursdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barThursdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barThursdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Friday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="barFridayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barFridayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barFridayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barFridayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barFridayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                           
                            <td width="61" align="center" valign="middle"><select name="barFridayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barFridayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barFridayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barFridayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barFridayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Saturday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="barSaturdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barSaturdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                            <td width="39" align="center" valign="middle">
							 <select name="barSaturdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barSaturdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barSaturdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="barSaturdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barSaturdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barSaturdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barSaturdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barSaturdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Sunday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="barSundayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barSundayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                            <td width="39" align="center" valign="middle">
							 <select name="barSundayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barSundayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barSundayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="barSundayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['barSundayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="barSundayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['barSundayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['barSundayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                    </table>
					</div>
                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="top">Payment Options : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_11','11')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_11" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_11','11');" class="delete"></a></div><p><?=$data_value_profe_info['paymentOptions']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                  <td width="70%" align="left" valign="middle">
       	        <table width="297" border="0" cellspacing="0" cellpadding="0">
				<?php
					$sql_select="select * from ".$prev."paymentoption order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					?>
                          <tr>
                            <td width="33" align="center" valign="middle"><input type="checkbox" name="paymentoptionCheck<?=$d_select['id']?>" <?php if(in_array($d_select['id'],$_SESSION['paymentoption'])){?> checked="checked" <?php }?> value="Y" /></td>
                            <td width="145" align="left" valign="middle"><?=$d_select['title']?></td>
                            <td width="119" align="center" valign="middle">&nbsp;</td>
                          </tr>
					<?php
					}
					?>	  
                          
                    </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">Delivery Available : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_12','12')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_12" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_12','12');" class="delete"></a></div><p><?=$data_value_profe_info['deliveryavailable']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">
                    <input name="deliveryAvaliablityCheck" type="radio" id="deliveryAvaliablityCheckYes" value="Y" <?php if($_SESSION['deliveryAvaliablityCheck']=='Y' ){?> checked="checked" <?php }?> onclick="checkOpenClose('deliveryAvaliablityCheckYes','deliveryAvaliablityDivID');" /> Yes 
                    <input name="deliveryAvaliablityCheck" id="deliveryAvaliablityCheckNo" type="radio" value="N" <?php if($_SESSION['deliveryAvaliablityCheck']=='N' || !$_SESSION['deliveryAvaliablityCheck'] ){?> checked="checked" <?php }?> onclick="checkOpenClose('deliveryAvaliablityCheckNo','deliveryAvaliablityDivID');"  /> No
                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                  <td width="70%" align="left" valign="middle">
				  <div id="deliveryAvaliablityDivID" <?php if($_SESSION['deliveryAvaliablityCheck']=='N' || !$_SESSION['deliveryAvaliablityCheck']){?> style="display:none" <?php }?>>
				  <table width="297" border="0" cellspacing="0" cellpadding="0">
				<tr>
                            <td colspan="5" align="left" valign="middle"><span>If Yes, Delivery Hours -</span></td>
                    </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Monday</td>
                            
                            <td width="59" align="center" valign="middle">
							<select name="deliverMondayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverMondayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                          <td width="39" align="center" valign="middle">
							 <select name="deliverMondayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverMondayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverMondayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverMondayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverMondayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverMondayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverMondayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverMondayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Tuesday</td>
                          
                            <td width="59" align="center" valign="middle">
							<select name="deliverTuesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverTuesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                           <td width="39" align="center" valign="middle">
							 <select name="deliverTuesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverTuesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverTuesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverTuesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverTuesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverTuesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverTuesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverTuesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Wednesday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="deliverWednesdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverWednesdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                           <td width="39" align="center" valign="middle">
							 <select name="deliverWednesdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverWednesdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverWednesdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverWednesdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverWednesdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverWednesdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverWednesdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverWednesdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Thursday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="deliverThursdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverThursdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverThursdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverThursdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverThursdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            
                            <td width="61" align="center" valign="middle"><select name="deliverThursdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverThursdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverThursdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverThursdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverThursdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Friday</td>
                          
                            <td width="59" align="center" valign="middle">
							<select name="deliverFridayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverFridayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                           <td width="39" align="center" valign="middle">
							 <select name="deliverFridayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverFridayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverFridayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverFridayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverFridayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverFridayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverFridayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverFridayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Saturday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="deliverSaturdayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverSaturdayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                           <td width="39" align="center" valign="middle">
							 <select name="deliverSaturdayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverSaturdayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverSaturdayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverSaturdayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverSaturdayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverSaturdayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverSaturdayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverSaturdayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                          <tr>
                            <td width="79" align="left" valign="middle">Sunday</td>
                           
                            <td width="59" align="center" valign="middle">
							<select name="deliverSundayTo" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverSundayTo']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
                            <td width="39" align="center" valign="middle">
							 <select name="deliverSundayToMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverSundayToMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverSundayToMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                            <td width="61" align="center" valign="middle"><select name="deliverSundayFrom" class="form_small_listmenu">
							<?php
							for($i=0;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>" <?php if($_SESSION['deliverSundayFrom']==$i){?> selected="selected" <?php }?>><?=number_format($i,2,':','')?></option>
							<?php
							}
							?>
							</select></td>
							<td width="39" align="center" valign="middle">
							 <select name="deliverSundayFromMeridian" class="form_small_listmenu" style="width:50px">
							
							
							<option value="AM" <?php if($_SESSION['deliverSundayFromMeridian']=='AM'){?> selected="selected" <?php }?>>AM</option>
							<option value="PM" <?php if($_SESSION['deliverSundayFromMeridian']=='PM'){?> selected="selected" <?php }?>>PM</option>
							
							</select>
						    </td>
                          </tr>
                    </table>
       	          </div>

                    </td>
                  </tr>
                  
                 
                  
                  <tr>
                    <th width="30%" align="left" valign="top">Already have an online ordering menu? : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_13','13')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_13" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_13','13');" class="delete"></a></div><p><?=$data_value_profe_info['haveanordermenu']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="top">
                    <input name="onlineorderCheck" id="onlineorderCheckYes" type="radio" value="Y" <?php if($_SESSION['onlineorderCheck']=='Y'){?> checked="checked" <?php }?> onclick="checkOpenClose('onlineorderCheckYes','onlineorderDivID');" /> Yes 
                    <input name="onlineorderCheck" id="onlineorderCheckNo" type="radio" value="N" <?php if($_SESSION['onlineorderCheck']=='N' || !$_SESSION['onlineorderCheck']){?> checked="checked" <?php }?> onclick="checkOpenClose('onlineorderCheckNo','onlineorderDivID');"  /> No
					
					<div id="onlineorderDivID" <?php if($_SESSION['onlineorderCheck']=='N'  || !$_SESSION['onlineorderCheck']){?> style="display:none" <?php }?>>
					<table cellpadding="0" cellspacing="0" border="0">
					 <tr>
                   
                    <td width="74%" align="left" valign="middle"><span>Online menu url -</span></td>
                  </tr>
                  
                  <tr>
                   
                    <td width="74%" align="left" valign="middle">http://<input name="onlineOrderUrl" value="<?=$_SESSION['onlineOrderUrl']?>" type="text" class="<?php if($_SESSION['onlineOrderUrl_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  
					</table>
					
					</div>
					
                    </td>
                  </tr>
                  
                 <tr>
                    <th width="30%" align="left" valign="middle">Will you be using our online takeout form : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_14','14')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_14" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_14','14');" class="delete"></a></div><p><?=$data_value_profe_info['usingouronolinetakeoutform']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="top">
                    <input name="takeoutFromCheck" type="radio" value="Y" <?php if($_SESSION['takeoutFromCheck']=='Y'){?> checked="checked" <?php }?> /> Yes 
                    <input name="takeoutFromCheck" type="radio" value="N" <?php if($_SESSION['takeoutFromCheck']=='N' || !$_SESSION['deliveryAvaliablityCheck'] ){?> checked="checked" <?php }?> /> No
                   </td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Price Range: <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_15','15')"    ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_15" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_15','15');" class="delete"></a></div><p><?=$data_value_profe_info['pricerange']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                  <td width="70%" align="left" valign="middle">
       	        <table width="297" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="25" align="right" valign="middle">$</td>
                            <td width="76" align="left" valign="middle">
							<select name="pricefrom" id="pricefrom" class="form_small_listmenu" onblur="setpricerange('pricefrom','priceTo','1');" onchange="setpricerange('pricefrom','priceTo','1');">
							<option value="0"></option>
							<?php
					$sql_select="select * from ".$prev."pricerange order by title ";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					?>
					<option value="<?=$d_select['title']?>" <?php if($d_select['title']==$_SESSION['pricefrom']){?> selected="selected" <?php }?> ><?=$d_select['title']?></option>
					<?php
					}
					?>
							
							
							</select>
							</td>
                            <td width="18" align="left" valign="middle">to</td>
							<td width="18" align="right" valign="middle">$</td>
                            <td width="160" align="left" valign="middle">
							<select name="priceTo" id="priceTo" class="form_small_listmenu"  onchange="setpricerange('pricefrom','priceTo','2');" >
							<option value="0"></option>
							<?php
					$sql_select="select * from ".$prev."pricerange order by title ";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					?>
					<option value="<?=$d_select['title']?>" <?php if($d_select['title']==$_SESSION['priceTo']){?> selected="selected" <?php }?> ><?=$d_select['title']?></option>
					<?php
					}
					?>
							
							
							</select></td>
                          </tr>
                    </table>

                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="top">Ambiance  : *<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_16','16')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_16" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_16','16');" class="delete"></a></div><p><?=$data_value_profe_info['ambalnce']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                  <td width="70%" align="left" valign="middle">
       	        <table width="297" border="0" cellspacing="0" cellpadding="0"><?php
					$n=0;
					$sql_select="select * from ".$prev."ambiance order by weight";
					$re_select=mysql_query($sql_select);
					while($d_select=mysql_fetch_array($re_select))
					{
					$n++;
					?><tr>
                            <td width="28" align="center" valign="middle"><input type="checkbox" name="ambianceCheck<?=$d_select['id']?>" <?php if(in_array($d_select['id'],$_SESSION['ambiance'])){?> checked="checked" <?php }?>  value="Y" /></td>
                            <td width="269" align="left" valign="middle"><?=$d_select['title']?></td>
                          </tr>
                          
					<?php
					}
					?>	
                        
                    </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">Speciality : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_17','17')"  ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_17" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_17','17');" class="delete"></a></div><p><?=$data_value_profe_info['specify']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="speciality" type="text" value="<?=$_SESSION['speciality']?>"  class="form_searchfield" /></td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="top">Restaurant Information : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_18','18')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_18" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_18','18');" class="delete"></a></div><p><?=$data_value_profe_info['resInfo']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><textarea name="information" cols="" rows="" class="form_textarea"><?=$_SESSION['information']?></textarea></td>
                  </tr>
   <tr>
                    <th width="30%" align="left" valign="middle">PDF For Menu : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_19','19')"  ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_19" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_19','19');" class="delete"></a></div><p><?=$data_value_profe_info['pdfofyourmenu']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input type="file" name="pdfForMenu" />
					<input type="hidden" name="pdfForMenupath" value="<?=$_SESSION['pdfForMenu_avoyz']?>" />
					<?php
					
					if($_SESSION['pdfForMenu_avoy']!='')
					{
					?>
					<a href="<?=$basepath?>download1.php?download_file=<?=$_SESSION['pdfForMenu_avoy']?>"><img src="images/pdf_icon.jpg" /></a>
					<?php
					}
					?>
					</td>
                  </tr>               
                  
     <tr>
                    <th width="30%" align="left" valign="top">Include a  logo 
for your listing: <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_8','8')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_8" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_8','8');" class="delete"></a></div><p><?=$data_value_profe_info['logoyourlist']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="top">
					<?php
				   if($_SESSION['logo_avoy']!='')
				   {
				   ?>
				   <img src="<?=$_SESSION['logo_avoy']?>" height="100" width="100" /> 
				  
				   <?php
				   }
				   ?>
  
   
   <input type="hidden" name="logopath" value="<?=$_SESSION['logo_avoy']?>" />
  
					<input name="logo" type="file" /></td>
                  </tr>             
                  <tr>
                    <th align="left" valign="middle"  colspan="2">Include images of your restaurant : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_20','20')" ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_20" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_20','20');" class="delete"></a></div><p><?=$data_value_profe_info['imageofyourrestaurant']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    
                  </tr>
				  <?php
				
				  $arr_pic=$_SESSION['arr_picture'];
				
				 
				 	 for($i=0;$i<7;$i++)
					{
					?>
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                    <td width="70%" align="left" valign="middle">
					<?php
					if($arr_pic[$i]!='')
					{
					?>
					<img src="<?=$basepath?><?=$arr_pic[$i]?>"  width="100" />
					<input type="hidden" name="picturepath<?=$i?>" value="<?=$arr_pic[$i]?>" />
					<?php
					}
					?>
					<input name="picture<?=$i?>" type="file" />
					</td>
                  </tr>
				  <?php
				  }
				  ?>
                  
                 
                  
				  
				   <tr>
                    <th width="30%" align="left" valign="middle">Website Url : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_21','21')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_21" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_21','21');" class="delete"></a></div><p><?=$data_value_profe_info['weburl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">http://
                    <input name="websiteUrl" type="text" value="<?=$_SESSION['websiteUrl']?>" class="form_searchfield" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Facebook Url : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_22','22')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_22" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_22','22');" class="delete"></a></div><p><?=$data_value_profe_info['facebookurl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">https://
                    <input name="facebookUrl" value="<?=$_SESSION['facebookUrl']?>" type="text" class="form_searchfield" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Twitter Url : <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_23','23')"  ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_23" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_23','23');" class="delete"></a></div><p><?=$data_value_profe_info['twetterurl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle">http://
                    <input name="twitterUrl"  value="<?=$_SESSION['twitterUrl']?>" type="text" class="form_searchfield" /></td>
                  </tr>
            </table>  
			<div style="margin:20px 0px;"><h5 style="text-align:left; margin-left:200px">Restaurant Owner Details &nbsp;<span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_24','24')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_24" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_24','24');" class="delete"></a></div><p><?=$data_value_profe_info['restaurantEmail']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></h5></div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_avoy">    
                  <tr>
                    <th width="30%" align="left" valign="middle">Choose a username : *</th>
                    <td width="70%" align="left" valign="middle"><input name="userName" type="text" value="<?=$_SESSION['userName']?>"  class="<?php if($_SESSION['userName_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Choose password : * </th>
                    <td width="70%" align="left" valign="middle"><input name="userpassword" value="<?=$_SESSION['userpassword']?>" type="password" class="<?php if($_SESSION['userpassword_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
				  <tr>
                    <th width="30%" align="left" valign="middle">Re-enter Password : * </th>
                    <td width="70%" align="left" valign="middle"><input name="conuserpassword" value="<?=$_SESSION['conuserpassword']?>" type="password" class="<?php if($_SESSION['conuserpassword_error']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  <tr>
                    <th width="30%" align="left" valign="middle">Account manager email ID : * </th>
                    <td width="70%" align="left" valign="middle"><input name="accountEmailID" value="<?=$_SESSION['accountEmailID']?>"  type="text" class="<?php if($_SESSION['join_EEerror']){?>form_searchfield_error<?php } else{?>form_searchfield<?php }?>" /></td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">Restaurant email ID : * <span style="position:relative"><a href="javascript:void(0);"   onclick="open_popup('popupID_25','25')"   ><img src="images/info_qstn.png" border="0" style="margin-top:-3px;"  /></a>
		  <div class="popup_div" id="popupID_25" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_25','25');" class="delete"></a></div><p><?=$data_value_profe_info['restaurantEmail']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
		  </span></th>
                    <td width="70%" align="left" valign="middle"><input name="emailID" type="text" value="<?=$_SESSION['emailID']?>" class="form_searchfield" /></td>
                  </tr>
                  
                  <tr>
                    <th width="30%" align="left" valign="middle">&nbsp;</th>
                    <td width="70%" align="left" valign="middle">
					<input type="hidden" name="type" value="2" id="type"  />
					<input type="hidden" name="zoneoffset" id="zoneoffset"  value="0" />
					<input name="" type="submit" value="submit" class="form_submit" /></td>
                  </tr>
                  
                </table>
			<?php
			}
			?>
            </div>
			
        </div>
    </div>
	
	<input  type="hidden" name="zoneoffset" id="zoneoffset" value="" />
	</form>
    
    <?php
			$sql_compareplan = "select * from ".$prev."compareplan ";
			$data_value_profe_extra=mysql_fetch_array(mysql_query($sql_compareplan));
	?>
	<div class="compare_plan_div" style="width:650px; margin:auto">
<h1>Compare Plans</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="50%" align="left" valign="top"></th>
    <th width="25%" align="center" valign="top"><h1>Basic<br /><font>Listing</font></h1></th>
    <th width="25%" align="center" valign="top"><h2>Professional<br /><font>Listing</font></h2></th>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Name<h1> <span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_26','26')">More details</a>
    
    <div class="popup_div" id="popupID_26" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_26','26');" class="delete"></a></div><p><?=$data_value_profe_extra['restaurentName']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span>
    </h1>
    
    
    </td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Logo
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_27','27')">More details</a>
    
    <div class="popup_div" id="popupID_27" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_27','27');" class="delete"></a></div><p><?=$data_value_profe_extra['restaurentLogo']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Address
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_28','28')">More details</a>
    
    <div class="popup_div" id="popupID_28" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_28','28');" class="delete"></a></div><p><?=$data_value_profe_extra['address']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Phone #
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_29','29')">More details</a>
    
    <div class="popup_div" id="popupID_29" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_29','29');" class="delete"></a></div><p><?=$data_value_profe_extra['resphone']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Cuisine Type
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_30','30')">More details</a>
    
    <div class="popup_div" id="popupID_30" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_30','30');" class="delete"></a></div><p><?=$data_value_profe_extra['rescusisine']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">Up to 2</td>
    <td width="25%" align="center" valign="middle">Up to 5</td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant URL
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_31','31')">More details</a>
    
    <div class="popup_div" id="popupID_31" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_31','31');" class="delete"></a></div><p><?=$data_value_profe_extra['resurl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Facebook Fan Page URL
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_32','32')">More details</a>
    
    <div class="popup_div" id="popupID_32" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_32','32');" class="delete"></a></div><p><?=$data_value_profe_extra['facefanurl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Twitter URL
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_33','33')">More details</a>
    
    <div class="popup_div" id="popupID_33" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_33','33');" class="delete"></a></div><p><?=$data_value_profe_extra['twitterurl']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Menu in PDF
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_34','34')">More details</a>
    
    <div class="popup_div" id="popupID_34" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_34','34');" class="delete"></a></div><p><?=$data_value_profe_extra['resmenupdf']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Use of Our Online Takeout Menu
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_35','35')">More details</a>
    
    <div class="popup_div" id="popupID_35" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_35','35');" class="delete"></a></div><p><?=$data_value_profe_extra['onlinetakeoutmenu']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Google Map Location
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_36','36')">More details</a>
    
    <div class="popup_div" id="popupID_36" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_36','36');" class="delete"></a></div><p><?=$data_value_profe_extra['googlemaplocaltion']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Hours
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_37','37')">More details</a>
    
    <div class="popup_div" id="popupID_37" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_37','37');" class="delete"></a></div><p><?=$data_value_profe_extra['restauranthour']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Restaurant Bar Hours
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_38','38')">More details</a>
    
    <div class="popup_div" id="popupID_38" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_38','38');" class="delete"></a></div><p><?=$data_value_profe_extra['restaurantbar']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Delivery Hours
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_39','39')">More details</a>
    
    <div class="popup_div" id="popupID_39" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_39','39');" class="delete"></a></div><p><?=$data_value_profe_extra['deliverhours']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Payment Options Available
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_40','40')">More details</a>
    
    <div class="popup_div" id="popupID_40" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_40','40');" class="delete"></a></div><p><?=$data_value_profe_extra['paymentoptionavailable']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Price Range
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_41','41')">More details</a>
    
    <div class="popup_div" id="popupID_41" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_41','41');" class="delete"></a></div><p><?=$data_value_profe_extra['pricerange']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Images of your Restaurant
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_42','42')">More details</a>
    
    <div class="popup_div" id="popupID_42" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_42','42');" class="delete"></a></div><p><?=$data_value_profe_extra['imageofrestaurant']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Link to Your Current Takeout Page
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_43','43')">More details</a>
    
    <div class="popup_div" id="popupID_43" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_43','43');" class="delete"></a></div><p><?=$data_value_profe_extra['linkcurrenttakeout']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="top">Reply to Customers Comments
      <h1><span style="position:relative"><a href="javascript:void(0)" class="more_link" onclick="open_popup('popupID_44','44')">More details</a>
    
    <div class="popup_div" id="popupID_44" style="display:none" >
<div class="pop_inner">
<div class="pop_anqr"></div>
<div class="delete_div"><a href="javascript:close_popup('popupID_44','44');" class="delete"></a></div><p><?=$data_value_profe_extra['replytocustomer']?></p> <div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
    </span></h1></td>
    <td width="25%" align="center" valign="middle">--</td>
    <td width="25%" align="center" valign="middle"><img src="images/tick.png" border="0" /></td>
  </tr>
  <tr>
    <th width="50%" align="left" valign="top"></th>
    <th width="25%" align="center" valign="top"><h2>Free<br /><font><a href="" class="more_link">Get Started</a></font></h2></th>
    <th width="25%" align="center" valign="top"><h2>$14.95<br /><font><a href="" class="more_link">Get Started</a></font></h2></th>
  </tr>
</table>


<div class="spacer"></div>
</div>
</div>



<div class="main_body_right">
<div class="padding_div">
<div class="deep_grey_pannel">
<div><h1>Already Have an Account?</h1>
<h2>Restaurant owners login</h2></div>


<div align="center"><input type="button" name="search" value="Login Here" class="login_button" onclick="javascript:window.location.href='ownerlogin.php'"  /></div>

<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>
	
    
	<div class="grey_pannel" style="margin-bottom:20px;">
    	<div class="right_content">
        	<h1>FAQ</h1>            
            <ul>
			<?php
			$sql_faqs="select * from ".$prev."faq order by weight ";
			$data_sql_faq=mysql_query($sql_faqs);
			while($row_faq=mysql_fetch_array($data_sql_faq))
			 {
?>
			
              <li style="cursor:pointer;" onclick="javascript:window.location.href='faqs.php#<?=$row_faq['id']?>'"><?=$row_faq['title']?></li>

<?php
			}
?>


            </ul>
            

            
            <div align="right"><a href="faqs.php" class="link_text">More FAQ's</a></div>
        </div>
    </div>
</div>


<div class="spacer"></div>
</div>
<!--END-MAIN_BODY-->



<!--FOOTER-->
<?php include("include/footer.php")?>
<!--END-FOOTER-->

<?php

unset($_SESSION['restaurantName_error']);
unset($_SESSION['phoneNumber_error']);
unset($_SESSION['address_error']);
unset($_SESSION['city_error']);
unset($_SESSION['country_error']);
unset($_SESSION['state_error']);
unset($_SESSION['cuisine_error']);
unset($_SESSION['postalCode_error']);
unset($_SESSION['join_EEerror']);
unset($_SESSION['userName_error']);
unset($_SESSION['userpassword_error']);

unset($_SESSION['restaurantName']);
unset($_SESSION['phoneNumber']);
unset($_SESSION['address']);
unset($_SESSION['addressline']);
unset($_SESSION['city']);
unset($_SESSION['country']);
unset($_SESSION['state']);
unset($_SESSION['postalCode']);
unset($_SESSION['cuisine']);

unset($_SESSION['dinningMondayTo']);
unset($_SESSION['dinningMondayFrom']);
unset($_SESSION['dinningTuesdayTo']);
unset($_SESSION['dinningTuesdayFrom']);
unset($_SESSION['dinningWednesdayTo']);
unset($_SESSION['dinningWednesdayFrom']);
unset($_SESSION['dinningThursdayTo']);
unset($_SESSION['dinningThursdayFrom']);
unset($_SESSION['dinningFridayTo']);
unset($_SESSION['dinningFridayFrom']);
unset($_SESSION['dinningSaturdayTo']);
unset($_SESSION['dinningSaturdayFrom']);
unset($_SESSION['dinningSundayTo']);
unset($_SESSION['dinningSundayFrom']);

unset($_SESSION['barAvailabiliyCheck']);

unset($_SESSION['barMondayTo']);
unset($_SESSION['barMondayFrom']);
unset($_SESSION['barTuesdayTo']);
unset($_SESSION['barTuesdayFrom']);
unset($_SESSION['barWednesdayTo']);
unset($_SESSION['barWednesdayFrom']);
unset($_SESSION['barThursdayTo']);
unset($_SESSION['barThursdayFrom']);
unset($_SESSION['barFridayTo']);
unset($_SESSION['barFridayFrom']);
unset($_SESSION['barSaturdayTo']);
unset($_SESSION['barSaturdayFrom']);
unset($_SESSION['barSundayTo']);
unset($_SESSION['barSundayFrom']);

unset($_SESSION['websiteUrl']);
unset($_SESSION['facebookUrl']);
unset($_SESSION['twitterUrl']);
unset($_SESSION['onlineorderCheck']);
unset($_SESSION['onlineOrderUrl']);
unset($_SESSION['pricefrom']);
unset($_SESSION['priceTo']);
unset($_SESSION['speciality']);
unset($_SESSION['information']);
unset($_SESSION['emailID']);
unset($_SESSION['userName']);
unset($_SESSION['userpassword']);
unset($_SESSION['accountEmailID']);




unset($_SESSION['deliveryAvaliablityCheck']);

unset($_SESSION['deliverMondayTo']);
unset($_SESSION['deliverMondayFrom']);
unset($_SESSION['deliverTuesdayTo']);
unset($_SESSION['deliverTuesdayFrom']);
unset($_SESSION['deliverWednesdayTo']);
unset($_SESSION['deliverWednesdayFrom']);
unset($_SESSION['deliverThursdayTo']);
unset($_SESSION['deliverThursdayFrom']);
unset($_SESSION['deliverFridayTo']);
unset($_SESSION['deliverFridayFrom']);
unset($_SESSION['deliverSaturdayTo']);
unset($_SESSION['deliverSaturdayFrom']);
unset($_SESSION['deliverSundayTo']);
unset($_SESSION['deliverSundayFrom']);
unset($_SESSION['arr_picture']);
unset($_SESSION['ambiance']);
unset($_SESSION['paymentoption']);
unset($_SESSION['logo_avoy']);
unset($_SESSION['takeoutFromCheck']);


unset($_SESSION['dinningMondayToMeridian']);
unset($_SESSION['dinningMondayFromMeridian']);
unset($_SESSION['dinningTuesdayToMeridian']);
unset($_SESSION['dinningTuesdayFromMeridian']);
unset($_SESSION['dinningWednesdayToMeridian']);
unset($_SESSION['dinningWednesdayFromMeridian']);
unset($_SESSION['dinningThursdayToMeridian']);
unset($_SESSION['dinningThursdayFromMeridian']);
unset($_SESSION['dinningFridayToMeridian']);
unset($_SESSION['dinningFridayFromMeridian']);
unset($_SESSION['dinningSaturdayToMeridian']);
unset($_SESSION['dinningSaturdayFromMeridian']);
unset($_SESSION['dinningSundayToMeridian']);
unset($_SESSION['dinningSundayFromMeridian']);

unset($_SESSION['barMondayToMeridian']);
unset($_SESSION['barMondayFromMeridian']);
unset($_SESSION['barTuesdayToMeridian']);
unset($_SESSION['barTuesdayFromMeridian']);
unset($_SESSION['barWednesdayToMeridian']);
unset($_SESSION['barWednesdayFromMeridian']);
unset($_SESSION['barThursdayToMeridian']);
unset($_SESSION['barThursdayFromMeridian']);
unset($_SESSION['barFridayToMeridian']);
unset($_SESSION['barFridayFromMeridian']);
unset($_SESSION['barSaturdayToMeridian']);
unset($_SESSION['barSaturdayFromMeridian']);
unset($_SESSION['barSundayToMeridian']);
unset($_SESSION['barSundayFromMeridian']);


unset($_SESSION['deliverMondayToMeridian']);
unset($_SESSION['deliverMondayFromMeridian']);
unset($_SESSION['deliverTuesdayToMeridian']);
unset($_SESSION['deliverTuesdayFromMeridian']);
unset($_SESSION['deliverWednesdayToMeridian']);
unset($_SESSION['deliverWednesdayFromMeridian']);
unset($_SESSION['deliverThursdayToMeridian']);
unset($_SESSION['deliverThursdayFromMeridian']);
unset($_SESSION['deliverFridayToMeridian']);
unset($_SESSION['deliverFridayFromMeridian']);
unset($_SESSION['deliverSaturdayToMeridian']);
unset($_SESSION['deliverSaturdayFromMeridian']);
unset($_SESSION['deliverSundayToMeridian']);
unset($_SESSION['deliverSundayFromMeridian']);
unset($_SESSION['conuserpassword']);
unset($_SESSION['pdfForMenu_avoy']);

?>




</body>
</html>
