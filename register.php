<?php
include("conn.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome To Takeout Option</title>
<link href="css/takeout.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript">
function myFocus(element) {
    if (element.value == element.defaultValue) {
      element.value = '';
    }
  }
   function myBlur(element) {
     if (element.value == '') {
       element.value = element.defaultValue;
    }
}
</script>
<script type="text/javascript">
function switchcontent(val,divid)
{
if(val=='HM')
{
document.getElementById(divid).style.display='none'
}
else if(val=='OF')
{
document.getElementById(divid).style.display='none'
}
else if(val=='OT')
{
document.getElementById(divid).style.display='';
}
}
</script>

<script src="js/general.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="autocompleteaddtional/jquery.autocomplete.css" />
<script type="text/javascript" src="autocompleteaddtional/jquery.js"></script>
<script type="text/javascript" src="autocompleteaddtional/jquery.autocomplete.js"></script>
<script>
$(document).ready(function(){
 $("#cityID").autocomplete("autocomplete.php", {
		selectFirst: true
	});
});

</script>
</head>

<body onload="<?php if($_SESSION['addressLevel']=='OT'){?>switchcontent('<?=$_SESSION['addressLevel']?>','otherID'); <?php }?>">



<?php include("include/header.php")?>




	


<!--MAIN_BODY-->
<div id="main_body">

<div class="main_body_left">
<div class="padding_div">
<div class="faqs_headline"><h3>Create <font>Account -</font></h3></div>

<form name="register" method="post" action="insert_register.php">
<div class="contact_div">
 <?php
					if(!empty($_SESSION['registration_failed']))
					{
						
						?>
						<div class="warnings">
						<h1>Registration Errors</h1>
						<ol>
						<?php
						if($_SESSION['fullName_error']=='100')
						{
						unset($_SESSION['fullName_error']);
						?>
						<li>Full Name is required.</li>
			
						<?php
						}
						if($_SESSION['streetAddress_error']=='100')
						{
						unset($_SESSION['streetAddress_error']);
						?>
						<li>Street Address  field is required.</li>
			
						<?php
						}
						if($_SESSION['zipCode_error']=='100')
						{
						unset($_SESSION['zipCode_error']);
						?>
						<li>ZIP  field is required.</li>
			
						<?php
						}
						elseif($_SESSION['state_error']=='100')
						{
						unset($_SESSION['state_error']);
						?>
						<li>State field is required.</li>
			
						<?php
						}
						elseif($_SESSION['city_error']=='100')
						{
						unset($_SESSION['city_error']);
						?>
						<li>City field is required.</li>
			
						<?php
						}
						if($_SESSION['phoneNumber_error']=='100')
						{
						unset($_SESSION['phoneNumber_error']);
						?>
						<li>Phone No field is required.</li>
			
						<?php
						}
						if(!empty($_SESSION['phoneNumber_error'])=='200')
						{
						unset($_SESSION['phoneNumber_error']);
						?>
						<li>Please use correct format for Phone No. e.g: 415-444-4444</li>
			
						<?php
						}
						if($_SESSION['join_EEerror']=='100')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>The Email field is required.</li>
			
						<?php
						}
						elseif($_SESSION['join_EEerror']=='200')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>Please Check  Email Address.</li>
			
						<?php
						}
						elseif($_SESSION['join_EEerror']=='300')
						{
						unset($_SESSION['join_EEerror']);
						?>
						<li>The Email Address "<?=$_SESSION['emailAddress']?>" is already registered with us. Please try with another one. </li>
			
						<?php
						}
						if($_SESSION['userPassword_error']=='100')
						{
						unset($_SESSION['userPassword_error']);
						?>
						<li>Password field is required. </li>
						<?php
						}
						
						elseif($_SESSION['userPassword_error']=='200' || $_SESSION['userPassword_error']=='300'  )
						{
						unset($_SESSION['userPassword_error']);
						?>
						<li>Please Re-enter Password </li>
			
						<?php
						}
						elseif( $_SESSION['userPassword_error']=='300'  )
						{
						unset($_SESSION['userPassword_error']);
						?>
						<li>Password and Re-enter Password Mismatch!!</li>
			
						<?php
						}
						
						?>
						</ol>
						</div>
						<?php
						unset($_SESSION['registration_failed']);
					}
					
					?>
<div class="faqs_questions_headline"><h2><font>Contact Information -</font></h2></div>
<div class="register_form_div">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="aaa_form">
  <tr>
    <th width="23%" align="left" valign="middle">Full Name: <span>*</span></th>
    <td width="77%" align="left" valign="top"><input name="fullName"  type="text" value="<?=stripslashes(isset($_SESSION['fullName']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="middle">Street Address: <span>*</span></th>
    <td align="left" valign="top"><input name="streetAddress" type="text" value="<?=stripslashes(isset($_SESSION['streetAddress']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="middle">Apt/Suite/Building:</th>
    <td align="left" valign="top"><input name="building" type="text" value="<?=stripslashes(isset($_SESSION['building']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="middle">Cross Street:</th>
    <td align="left" valign="top"><input name="crossStreet" type="text" value="<?=stripslashes(isset($_SESSION['crossStreet']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="middle">ZIP: <span>*</span></th>
    <td align="left" valign="top"><input name="zipCode" type="text" value="<?=stripslashes(isset($_SESSION['zipCode']))?>" class="subscribe_text_area" style="width:220px;" />
	<div  id="print_eroorDIV" style="height:14px;"></div>
	</td>
  </tr>
  <tr>
    <th align="left" valign="middle">State: <span>*</span></th>
    <td align="left" valign="top">
	<select name="state" id="state" class="<?php if(isset($_SESSION['state_error'])){?>form_listmenu_error<?php } else{?>form_listmenu<?php }?>"   style="width:220px;">
				<option value="0"></option>		
				<?php	
				 $sql_select="select * from ".$prev."state where country='1' order by weight";
				 $re_select=mysql_query($sql_select);
				 while($d_select=@mysql_fetch_array($re_select))
				 {
				  ?>
				  <option  value="<?=$d_select['id']?>" <?php if(isset($_SESSION['state'])==$d_select['id']){?> selected="selected"<?php }?>><?=$d_select['title']?></option>
				<?php
				}
				?>	
				</select>
	<div  id="print_eroorDIV" style="height:14px;"></div>
	</td>
  </tr>
  <tr>
    <th align="left" valign="middle">City: <span>*</span></th>
    <td align="left" valign="top"><input name="city" id="cityID" type="text" value="<?=stripslashes(isset($_SESSION['city']))?>" class="subscribe_text_area" style="width:220px;" /></td>
  </tr>
  <tr>
    <th align="left" valign="top">Phone No.: <span>*</span></th>
    <td align="left" valign="top"><input name="phoneNumber" type="text" value="<?=stripslashes(isset($_SESSION['phoneNumber']))?>" class="subscribe_text_area" style="width:220px;"/><h5>e.g: 415-444-4444</h5></td>
  </tr>
  <tr>
    <td align="left" valign="middle">Address Label:</td>
    <td align="left" valign="top">
      <table width="300" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><input type="radio" name="addressLevel" value="HM" id="RadioGroup1_0" <?php if(isset($_SESSION['addressLevel'])=='HM' ){?> checked="checked" <?php } ?> onclick="javascript:switchcontent('HM','otherID');" /><label for="RadioGroup1_0">Home</label></td>
          <td><input type="radio" name="addressLevel" value="OF" id="RadioGroup1_1" onclick="javascript:switchcontent('OF','otherID');" <?php if(isset($_SESSION['addressLevel'])=='OF' ){?> checked="checked" <?php }?> /><label for="RadioGroup1_1">Office</label></td>
          <td><input type="radio" name="addressLevel" value="OT" id="RadioGroup1_2" onclick="javascript:switchcontent('OT','otherID');" <?php if(isset($_SESSION['addressLevel'])=='OT' ){?> checked="checked" <?php }?>  /><label for="RadioGroup1_2">Other</label></td>
		  </tr>
            </table>
    </td>
  </tr>
  <tr id="otherID" style="display:none">
    <td align="left" valign="middle"></td>
    <td align="left" valign="top"><input name="otherAddress" type="text" value="<?=isset($_SESSION['otherAddress'])?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  
</table>


<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>

<div class="register_form_div">
<div class="login_icon"><h2><font>Login Information -</font></h2></div>
<div class="contact_div">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="aaa_form">
  <tr>
    <th width="23%" align="left" valign="top">Email: <span>*</span></th>
    <td width="77%"><input name="emailAddress" type="text" value="<?=stripslashes(isset($_SESSION['emailAddress']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="top">Password: <span>*</span></th>
    <td><input name="userPassword" type="password" value="<?=stripslashes(isset($_SESSION['userPassword']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <th align="left" valign="top">Re-enter Password : <span>*</span></th>
    <td><input name="userConPassword" type="password" value="<?=stripslashes(isset($_SESSION['userConPassword']))?>" class="subscribe_text_area" style="width:300px;"/></td>
  </tr>
  <tr>
    <td align="left" valign="top" colspan="2"><input name="" type="checkbox" value="" checked id="receive" /><label for="receive">Yes, I would like to receive emails with deals and discount codes from TakeoutOptions.com</label></td>
  </tr>
  <tr>
    <td></td>
    <td>
	<input type="hidden" name="ziperror" id="ziperror" value="<?=$_SESSION['ziperror']?>" />
	<input type="submit" name="search" value="CONTINUE" class="meroon_submit_button" /></td>
  </tr>
</table>


<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>


</form>
<div class="spacer"></div>
</div>



<div class="spacer"></div>
</div>



<div class="main_body_right">
<?php include("include/newsletter.php")?>

<div class="padding_div">
<div class="order_div">
<h1>NOW GET <font>20%</font> OFF</h1>
<h2>On Your First Order</h2>
<h3><font><a href="" class="order_link">Click here</a></font> to start ordering</h3>
Visit us: www.takeoutoption.com
<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>

<div class="padding_div">
<div class="grey_pannel"  align="center">
<?php
$sql_adsence="select * from ".$prev."adsence where advertisementposition='Register' order by rand() ";
$re_adsence=mysql_query($sql_adsence);
$d_adsence=mysql_fetch_array($re_adsence);

?>
<?php
	if($d_adsence['adType']=='G')
	{
		echo htmlspecialchars_decode($d_adsence['adsenseCde']);
	}
	else
	{
	?>
	<a href="http://<?=$d_adsence['link']?>" target="_blank"><img src="<?=$basepath?><?=$d_adsence['imagefile']?>" border="0"  /></a>
	<?php
	}
	?>
<div class="spacer"></div>
</div>
<div class="spacer"></div>
</div>

<div class="spacer"></div>
</div>
<div class="spacer"></div>
<?php include("include/cuisine_panel.php")?>
<div class="spacer"></div>
</div>
<!--END-MAIN_BODY-->



<!--FOOTER-->
<?php include("include/footer.php")?>
<!--END-FOOTER-->

<?php

unset($_SESSION['emailAddress']);
unset($_SESSION['fullName']);
unset($_SESSION['streetAddress']);
unset($_SESSION['building']);
unset($_SESSION['crossStreet']);
unset($_SESSION['zipCode']);
unset($_SESSION['city']);
unset($_SESSION['state']);
unset($_SESSION['phoneNumber']);
unset($_SESSION['addressLevel']);
unset($_SESSION['otherAddress']);
unset($_SESSION['userPassword']);
unset($_SESSION['userConPassword']);
unset($_SESSION['emailAddress']);
?>




</body>
</html>
